<?php

return [
    'url' => env('COINDESK_API', 'https://api.coindesk.com/v1/bpi/'),
];