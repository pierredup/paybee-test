<?php

return [
    'api' => [
        'base_uri' => env('TELEGRAM_API', 'https://api.telegram.org/bot'),
        'headers' => [
            'Accept' => 'application/json'
        ]
    ],
    'auth_token' => env('TELEGRAM_AUTH_TOKEN'),
];