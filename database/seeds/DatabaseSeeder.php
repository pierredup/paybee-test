<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('users');

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $table->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt($faker->password),
                'admin' => false,
            ]);
        }

        // $this->call(UsersTableSeeder::class);
    }
}
