<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminColumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('admin')->default(false);
        });

        DB::table('users')->insert([
            'name' => 'PayBee Admin',
            'email' => 'admin@pay.bee',
            'password' => '$2y$10$ce0LTUbuZdWir6j5uP.GweLpT88efz0O3.Mgdo8BHwLToUjuTpkc2', // pre-encrypted password. There should probably be a better way to create an initial user with a password without hard-coding the password hash here
            'admin' => true,
            'created_at' => \Carbon\Carbon::now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('admin');
        });
    }
}
