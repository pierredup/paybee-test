<?php

namespace Tests\Api;

use App\Api\Telegram as TelegramApi;
use App\Exception\TelegramRequestException;
use Faker\Factory;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class TelegramTest extends \PHPUnit_Framework_TestCase
{
    public function testSendMessage()
    {
        $faker = Factory::create();

        $chatId = $faker->randomNumber(8);
        $text = $faker->text(50);

        $mock = new MockHandler([
            new Response(200)
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $telegram = new TelegramApi($client);

        $telegram->sendMessage($chatId, $text);
    }

    /**
     * @expectedException \App\Exception\TelegramRequestException
     * @expectedExceptionMessage Telegram API Exception: Bad Request: chat not found
     * @expectedExceptionCode 400
     */
    public function testSendMessageFail()
    {
        $faker = Factory::create();

        $mock = new MockHandler([
            new TelegramRequestException(new Request('GET', 'sendMessage'), new Response(400, [], '{"ok" : false, "error_code" : 400, "description" : "Bad Request: chat not found"}'))
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $telegram = new TelegramApi($client);

        $chatId = $faker->randomNumber(8);
        $text = $faker->text(50);

        $telegram->sendMessage($chatId, $text);
    }
}
