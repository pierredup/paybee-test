<?php

namespace Tests\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Handler\BotCommands\StartCommand;
use App\Integration\Telegram\Model\ChatMessage;
use App\Integration\Telegram\Model\FromUser;
use App\Integration\Telegram\Model\Message;
use App\Integration\Telegram\Model\Type;
use App\Integration\Telegram\Model\Update;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Query\Builder;
use Mockery as M;

class StartCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testExecuteNotLinked()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('telegram_id', '=', $fromUser)
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn(null);

        $command = new StartCommand($qb, 'http://localhost');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/start');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 6]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, str_replace('{URL}', 'http://localhost', StartCommand::INSTRUCTIONS_TEXT));

        $command->execute($update);
    }

    public function testExecuteLinked()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('telegram_id', '=', $fromUser)
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn((object) ['id' => $this->faker->randomNumber()]);

        $command = new StartCommand($qb, 'http://localhost');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/start');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 6]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->never()
            ->with($chatId, str_replace('{URL}', 'http://localhost', StartCommand::INSTRUCTIONS_TEXT));

        $command->execute($update);
    }
}
