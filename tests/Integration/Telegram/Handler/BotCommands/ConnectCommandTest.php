<?php

namespace Tests\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Handler\BotCommands\ConnectCommand;
use App\Integration\Telegram\Model\ChatMessage;
use App\Integration\Telegram\Model\FromUser;
use App\Integration\Telegram\Model\Message;
use App\Integration\Telegram\Model\Type;
use App\Integration\Telegram\Model\Update;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Query\Builder;
use Mockery as M;

class ConnectCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testExecuteEmptyAuthCode()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $command = new ConnectCommand($qb);

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/connect');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 8]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Authentication code missing or invalid format.');

        $command->execute($update);
    }

    public function testExecuteInvalidAuthCodeSyntax()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $command = new ConnectCommand($qb);

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/connect one');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 8]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Authentication code missing or invalid format.');

        $command->execute($update);
    }

    public function testExecuteInvalidAuthCode()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('token', '=', 'onetwoth')
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn(null);

        $command = new ConnectCommand($qb);

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/connect onetwoth');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 8]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Authentication code is invalid');

        $command->execute($update);
    }

    public function testExecuteAccountAlreadyLinked()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('token', '=', 'onetwoth')
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn((object) ['id' => $this->faker->randomNumber(), 'telegram_id' => $this->faker->randomNumber()]);

        $command = new ConnectCommand($qb);

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/connect onetwoth');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 8]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Account already connected');

        $command->execute($update);
    }

    public function testExecuteLinkAccount()
    {
        $fromUser = $this->faker->randomNumber(8);
        $userId = $this->faker->randomNumber();

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('token', '=', 'onetwoth')
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn((object) ['id' => $userId, 'telegram_id' => null]);

        $qb->shouldReceive('where')
            ->once()
            ->with('id', '=', $userId)
            ->andReturnSelf();

        $qb->shouldReceive('update')
            ->once()
            ->with(['telegram_id' => $fromUser]);

        $command = new ConnectCommand($qb);

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/connect onetwoth');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 8]));
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Account successfully connected');

        $command->execute($update);
    }
}
