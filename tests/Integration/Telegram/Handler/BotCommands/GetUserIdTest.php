<?php

namespace Tests\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Handler\BotCommands\GetUserId;
use App\Integration\Telegram\Model\ChatMessage;
use App\Integration\Telegram\Model\FromUser;
use App\Integration\Telegram\Model\Message;
use App\Integration\Telegram\Model\Update;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Query\Builder;
use Mockery as M;

class GetUserIdTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testExecuteNotLinked()
    {
        $fromUser = $this->faker->randomNumber(8);

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('telegram_id', '=', $fromUser)
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn(null);

        $command = new GetUserId($qb, 'http://localhost');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Your account is not linked to PayBee. Send the command /start and follow the instructions to get link your account');

        $command->execute($update);
    }

    public function testExecute()
    {
        $fromUser = $this->faker->randomNumber(8);

        $userId = $this->faker->randomNumber();

        $qb = M::mock(Builder::class);

        $qb->shouldReceive('where')
            ->once()
            ->with('telegram_id', '=', $fromUser)
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn((object) ['id' => $userId]);

        $command = new GetUserId($qb, 'http://localhost');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setChat($chat);

        $from = new FromUser();
        $from->setId($fromUser);
        $message->setFrom($from);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, $userId);

        $command->execute($update);
    }
}
