<?php

namespace Tests\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Handler\BotCommands\GetBTCEquivalent;
use App\Integration\Telegram\Model\ChatMessage;
use App\Integration\Telegram\Model\Message;
use App\Integration\Telegram\Model\Type;
use App\Integration\Telegram\Model\Update;
use Faker\Factory;
use Faker\Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mockery as M;

class GetBTCEquivalentTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testExecuteEmptyCommand()
    {
        $command = new GetBTCEquivalent(M::mock(Client::class), 'USD');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/getbtcequivalent');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 18]));
        $message->setChat($chat);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'Command cannot be empty');

        $command->execute($update);
    }

    public function testExecuteInvalidSyntax()
    {
        $command = new GetBTCEquivalent(M::mock(Client::class), 'USD');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/getbtcequivalent one two three');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 18]));
        $message->setChat($chat);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, '"Command syntax is invalid. Please use /getbtcequivalent {amount} {currency}"');

        $command->execute($update);
    }

    public function testExecuteFailedToGetRates()
    {
        $mock = new MockHandler([
            new RequestException('', new Request('GET', 'usd.json'), new Response(500, [], 'Sorry, your requested currency ONE is not supported or is invalid')),
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $command = new GetBTCEquivalent($client, 'USD');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/getbtcequivalent 3000 one');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 18]));
        $message->setChat($chat);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, 'An error occurred while fetching BTC rates - Sorry, your requested currency ONE is not supported or is invalid');

        $command->execute($update);
    }

    public function testExecute()
    {
        $mock = new MockHandler([
            new Response(200, [], '{"time":{"updated":"Dec 16, 2016 17:56:00 UTC","updatedISO":"2016-12-16T17:56:00+00:00","updateduk":"Dec 16, 2016 at 17:56 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","rate":"780.0188","description":"United States Dollar","rate_float":780.0188}}}'),
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $command = new GetBTCEquivalent($client, 'USD');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/getbtcequivalent 30 USD');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 18]));
        $message->setChat($chat);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, '30 USD is 0.0385 BTC (780.02 USD - 1 BTC)');

        $command->execute($update);
    }

    public function testExecuteDefaultCurrency()
    {
        $mock = new MockHandler([
            new Response(200, [], '{"time":{"updated":"Dec 16, 2016 17:56:00 UTC","updatedISO":"2016-12-16T17:56:00+00:00","updateduk":"Dec 16, 2016 at 17:56 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","rate":"780.0188","description":"United States Dollar","rate_float":780.0188}}}'),
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $command = new GetBTCEquivalent($client, 'USD');

        $chatId = $this->faker->randomNumber(8);
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setText('/getbtcequivalent 30');
        $message->setType(new Type(['type' => 'bot_command', 'offset' => 0, 'length' => 18]));
        $message->setChat($chat);

        $update = new Update();
        $update->setMessage($message);

        Telegram::shouldReceive('sendMessage')
            ->once()
            ->with($chatId, '30 USD is 0.0385 BTC (780.02 USD - 1 BTC)');

        $command->execute($update);
    }
}
