<?php

namespace Tests\Integration\Telegram\Handler;

use App\Integration\Telegram\Handler\BotCommandHandler;
use App\Integration\Telegram\Handler\BotCommands\CommandInterface;
use App\Integration\Telegram\Model\ChatMessage;
use App\Integration\Telegram\Model\Message;
use App\Integration\Telegram\Model\Type;
use App\Integration\Telegram\Model\Update;
use Faker\Factory;
use Faker\Generator;
use Mockery as M;

class BotCommandHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testSupportsBotCommand()
    {
        $handler = new BotCommandHandler();

        $this->assertSame('bot_command', $handler->getType());
    }

    public function testExecute()
    {
        $chatId = $this->faker->randomNumber(8);
        $text = $this->faker->text(50);

        $update = new Update();
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setChat($chat);
        $message->setType(new Type(['type' => 'mention', 'offset' => 0, 'length' => 17]));
        $message->setText('/getbtcequivalent '.$text);
        $update->setMessage($message);

        $command = M::mock(CommandInterface::class);
        $command->shouldReceive('execute')
            ->once()
            ->with($update);

        $handler = new BotCommandHandler(['getbtcequivalent' => $command]);

        $this->assertTrue($handler->handle($update));
    }

    public function testFailedExecute()
    {
        $chatId = $this->faker->randomNumber(8);
        $text = $this->faker->text(50);

        $update = new Update();
        $chat = new ChatMessage();
        $chat->setId($chatId);

        $message = new Message();
        $message->setChat($chat);
        $message->setType(new Type(['type' => 'mention', 'offset' => 0, 'length' => 17]));
        $message->setText('/getbtcequivalent '.$text);
        $update->setMessage($message);

        $command = M::mock(CommandInterface::class);
        $command->shouldReceive('execute')
            ->once()
            ->andThrow(\Exception::class);

        $handler = new BotCommandHandler(['getbtcequivalent' => $command]);

        $this->assertFalse($handler->handle($update));
    }
}