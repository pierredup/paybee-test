<?php

namespace Tests\Integration\Telegram;

use App\Integration\Telegram\Handler\BotCommandHandler;
use App\Integration\Telegram\Model\Message;
use App\Integration\Telegram\Model\Type;
use App\Integration\Telegram\Model\Update;
use App\Integration\Telegram\WebHook;
use Mockery as M;

class WebHookTest extends \PHPUnit_Framework_TestCase
{
    public function testHandle()
    {
        $botCommand = M::mock(BotCommandHandler::class);

        $botCommand->shouldReceive('getType')
            ->once()
            ->withNoArgs()
            ->andReturn('bot_command');

        $message = new Message();
        $message->setType(new Type(['type' => 'bot_command']));

        $update = new Update();
        $update->setMessage($message);

        $handler = new WebHook();
        $handler->addHandler($botCommand);

        $botCommand->shouldReceive('handle')
            ->once()
            ->with($update)
            ->andReturn(true);

        $handler->handle($update);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage There are no handler available to handle "mention" updates
     */
    public function testHandleFail()
    {
        $botCommand = M::mock(BotCommandHandler::class);
        $botCommand->shouldReceive('getType')
            ->once()
            ->withNoArgs()
            ->andReturn('bot_command');

        $message = new Message();
        $message->setType(new Type(['type' => 'mention']));

        $update = new Update();
        $update->setMessage($message);

        $handler = new WebHook();
        $handler->addHandler($botCommand);

        $botCommand->shouldReceive('handle')
            ->never();

        $handler->handle($update);
    }
}