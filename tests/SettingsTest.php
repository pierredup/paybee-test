<?php
/**
 * This file is part of the paybee-test project.
 *
 * @author     pierre
 * @copyright  Copyright (c) 2016
 */

namespace Tests;

use App\Settings;
use Faker\Factory;
use Illuminate\Database\Query\Builder;
use Mockery as M;

class SettingsTest extends \PHPUnit_Framework_TestCase
{
    public function testGet()
    {
        $qb = M::mock(Builder::class);
        $value = Factory::create()->text(10);

        $qb->shouldReceive('where')
            ->once()
            ->with('key', '=', 'config_key')
            ->andReturnSelf();

        $qb->shouldReceive('first')
            ->once()
            ->withNoArgs()
            ->andReturn((object) ['value' => $value]);

        $settings = new Settings($qb);

        $this->assertSame($value, $settings->get('config_key'));
    }

    public function testSet()
    {
        $qb = M::mock(Builder::class);
        $value = Factory::create()->text(10);

        $qb->shouldReceive('where')
            ->once()
            ->with('key', '=', 'config_key')
            ->andReturnSelf();

        $qb->shouldReceive('update')
            ->once()
            ->with(['value' => $value]);

        $settings = new Settings($qb);

        $settings->set('config_key', $value);
    }
}
