<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/bot-config', 'Admin\BotConfigController')->name('admin.bot_config');
Route::get('/bot-config/settings', 'Admin\SettingsController')->name('admin.settings');
Route::get('/home', 'HomeController');

Route::post('/bot-config/settings/save', 'Admin\SaveSettingsController')->name('admin.settings.save');
Route::post('/telegram/webhook', 'Telegram\WebHookController');

Auth::routes();

