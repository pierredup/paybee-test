<?php

namespace App\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Model\Update;
use Illuminate\Database\Query\Builder;

class GetUserId implements CommandInterface
{
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Update $update)
    {
        $chatId = $update->getMessage()->getChat()->getId();
        $fromUser = $update->getMessage()->getFrom()->getId();

        $user = $this->builder->where('telegram_id', '=', $fromUser)->first();

        if (!$user) {
            return $this->respond($chatId, 'Your account is not linked to PayBee. Send the command /start and follow the instructions to get link your account');
        }

        return $this->respond($chatId, $user->id);
    }

    private function respond(int $chatId, string $message): void
    {
        Telegram::sendMessage($chatId, $message);
    }
}