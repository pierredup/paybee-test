<?php

namespace App\Integration\Telegram\Handler\BotCommands;

use App\Integration\Telegram\Model\Update;

interface CommandInterface
{
    /**
     * Executes the command from the bot
     *
     * @param Update $update
     */
    public function execute(Update $update);
}