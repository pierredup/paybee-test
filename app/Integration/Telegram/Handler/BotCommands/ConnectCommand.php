<?php

namespace App\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Model\Update;
use Illuminate\Database\Query\Builder;

class ConnectCommand implements CommandInterface
{
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Update $update)
    {
        $chatId = $update->getMessage()->getChat()->getId();
        $fromUser = $update->getMessage()->getFrom()->getId();

        $authCode = $update->getMessage()->getTrimmedText();

        if (empty($authCode) || !preg_match('/[a-zA-Z0-9]{8}/', $authCode)) {
            return $this->respond($chatId, 'Authentication code missing or invalid format.');
        }

        $user = $this->builder->where('token', '=', $authCode)->first();

        if (!$user) {
            return $this->respond($chatId, 'Authentication code is invalid');
        }

        if (!empty($user->telegram_id)) {
            return $this->respond($chatId, 'Account already connected');
        }

        $this->builder->where('id', '=', $user->id)->update(['telegram_id' => $fromUser]);

        return $this->respond($chatId, 'Account successfully connected');
    }

    private function respond(int $chatId, string $message): void
    {
        Telegram::sendMessage($chatId, $message);
    }
}