<?php

namespace App\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Model\Update;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class GetBTCEquivalent implements CommandInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $defaultCurrency;

    public function __construct(Client $client, string $defaultCurrency)
    {
        $this->client = $client;
        $this->defaultCurrency = $defaultCurrency;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Update $update)
    {
        $chatId = $update->getMessage()->getChat()->getId();

        $commandName = $update->getMessage()->getTriggerText();
        $command = $update->getMessage()->getTrimmedText();

        if (empty($command)) {
            return $this->respond($chatId, 'Command cannot be empty');
        }

        if (!preg_match('/^(?<amount>[\d]+)\s?(?<currency>[a-zA-Z]{3})?$/', $command, $matches)) {
            return $this->respond($chatId, "Command syntax is invalid. Please use {$commandName} {amount} {currency}");
        }

        $currency = strtoupper($matches['currency'] ?? $this->defaultCurrency);
        $amount = (int) $matches['amount'];

        try {
            $rate = $this->fetchRate($currency);
        } catch (RequestException $e) {
            $message = 'An error occurred while fetching BTC rates';

            if ($e->hasResponse()) {
                $message .= ' - '.$e->getResponse()->getBody();
            }

            return $this->respond($chatId, $message);
        }

        $totalValue = $amount / $rate;

        $message = sprintf('%1$s %2$s is %3$.4f BTC (%4$.2f %2$s - 1 BTC)', $amount, $currency, $totalValue, round($rate, 2));

        return $this->respond($chatId, $message);
    }

    private function fetchRate(string $currency): float
    {
        $response = $this->client->get("currentprice/$currency.json");

        $bpi = json_decode((string) $response->getBody(), true);

        return (float) $bpi['bpi'][$currency]['rate_float'];
    }

    private function respond(int $chatId, string $message): void
    {
        Telegram::sendMessage($chatId, $message);
    }
}