<?php

namespace App\Integration\Telegram\Handler\BotCommands;

use App\Facades\Telegram;
use App\Integration\Telegram\Model\Update;
use Illuminate\Database\Query\Builder;

class StartCommand implements CommandInterface
{
    const INSTRUCTIONS_TEXT = <<<TEXT
You need to link your Telegram account to your PayBee account.
If you do not have a PayBee account, register at {URL}.

If you already have a PayBee account, log into your account at {URL} and follow the instructions to link your account.
TEXT;

    /**
     * @var Builder
     */
    private $builder;

    /**
     * @var string
     */
    private $baseUrl;

    public function __construct(Builder $builder, string $baseUrl)
    {
        $this->builder = $builder;
        $this->baseUrl = $baseUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Update $update)
    {
        $chatId = $update->getMessage()->getChat()->getId();
        $fromUser = $update->getMessage()->getFrom()->getId();

        $user = $this->builder->where('telegram_id', '=', $fromUser)->first();

        if (!$user) {
            return $this->respond($chatId, str_replace('{URL}', $this->baseUrl, self::INSTRUCTIONS_TEXT));
        }
    }

    private function respond(int $chatId, string $message): void
    {
        Telegram::sendMessage($chatId, $message);
    }
}