<?php

namespace App\Integration\Telegram\Handler;

use App\Integration\Telegram\Model\Update;

interface HandlerInterface
{
    /**
     * Get the type of update that this handle should handle
     *
     * @return bool|string
     */
    public function getType(): string;

    /**
     * Handles a specified update
     *
     * @param Update $update
     *
     * @return bool Whether the handle was successful
     */
    public function handle(Update $update): ?bool;
}