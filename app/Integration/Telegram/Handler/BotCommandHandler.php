<?php

namespace App\Integration\Telegram\Handler;

use App\Exception\TelegramRequestException;
use App\Integration\Telegram\Handler\BotCommands\CommandInterface;
use App\Integration\Telegram\Model\Update;
use Webmozart\Assert\Assert;

class BotCommandHandler implements HandlerInterface
{
    /**
     * @var CommandInterface[]
     */
    private $commands = [];

    public function __construct(array $commands = [])
    {
        foreach ($commands as $name => $command) {
            $this->add($name, $command);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return 'bot_command';
    }

    /**
     * {@inheritdoc}
     *
     * @throws TelegramRequestException
     */
    public function handle(Update $update): ?bool
    {
        $command = $this->getCommand($update);

        try {
            $command->execute($update);
        } catch (\Throwable $e) {
            return false;
        }

        return true;
    }

    /**
     * Add a command handler
     *
     * @param string           $name
     * @param CommandInterface $command
     */
    public function add(string $name, CommandInterface $command)
    {
        Assert::keyNotExists($this->commands, $name, sprintf('The command "%s" is already registered', $name));

        $this->commands[strtolower($name)] = $command;
    }

    private function getCommand(Update $update): CommandInterface
    {
        $command = strtolower(ltrim($update->getMessage()->getTriggerText(), '/'));

        Assert::keyExists($this->commands, $command, sprintf('No command is registered for "%s"', $command));

        return $this->commands[$command];
    }
}