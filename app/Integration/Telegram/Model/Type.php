<?php

namespace App\Integration\Telegram\Model;

use JMS\Serializer\Annotation as Serializer;

class Type
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $length;

    public function __construct(array $data = [])
    {
        $this->type = $data['type'] ?? '';
        $this->offset = $data['offset'] ?? 0;
        $this->length = $data['length'] ?? 0;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    public function __toString(): string
    {
        return $this->getType();
    }
}