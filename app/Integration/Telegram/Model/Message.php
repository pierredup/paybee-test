<?php

namespace App\Integration\Telegram\Model;

use JMS\Serializer\Annotation as Serializer;

class Message
{
    /**
     * @Serializer\SerializedName("message_id")
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $id;

    /**
     * @Serializer\Type("App\Integration\Telegram\Model\FromUser")
     *
     * @var FromUser
     */
    private $from;

    /**
     * @Serializer\Type("App\Integration\Telegram\Model\ChatMessage")
     *
     * @var ChatMessage
     */
    private $chat;

    /**
     * @Serializer\Type("DateTime<'U'>")
     *
     * @var \DateTime
     */
    private $date;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $text;

    /**
     * @Serializer\Type("App\Integration\Telegram\Model\Type")
     * @Serializer\SerializedName("entities")
     *
     * @var Type
     */
    private $type;

    public function __construct()
    {
        $this->type = new Type();
        $this->from = new FromUser();
        $this->chat = new ChatMessage();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return FromUser
     */
    public function getFrom(): FromUser
    {
        return $this->from;
    }

    /**
     * @return ChatMessage
     */
    public function getChat(): ChatMessage
    {
        return $this->chat;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTrimmedText(): string
    {
        return ltrim(substr($this->text, $this->type->getLength()), ' ');
    }

    /**
     * Gets the text tht triggered the update (I.E a mention, a command etc)
     *
     * @return string
     */
    public function getTriggerText(): string
    {
        return rtrim(substr($this->text, $this->type->getOffset(), $this->type->getLength()), ' ');
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @param int $id
     *
     * @return Message
     */
    public function setId(int $id): Message
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param FromUser $from
     *
     * @return Message
     */
    public function setFrom(FromUser $from): Message
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param ChatMessage $chat
     *
     * @return Message
     */
    public function setChat(ChatMessage $chat): Message
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate(\DateTime $date): Message
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @param string $text
     *
     * @return Message
     */
    public function setText(string $text): Message
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param Type $type
     *
     * @return Message
     */
    public function setType(Type $type): Message
    {
        $this->type = $type;

        return $this;
    }
}