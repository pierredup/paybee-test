<?php

namespace App\Integration\Telegram\Model;

use JMS\Serializer\Annotation as Serializer;

class FromUser
{
    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $id;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("first_name")
     *
     * @var string
     */
    private $firstName;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("last_name")
     *
     * @var string
     */
    private $lastName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param int $id
     *
     * @return FromUser
     */
    public function setId(int $id): FromUser
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $firstName
     *
     * @return FromUser
     */
    public function setFirstName(string $firstName): FromUser
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param string $lastName
     *
     * @return FromUser
     */
    public function setLastName(string $lastName): FromUser
    {
        $this->lastName = $lastName;

        return $this;
    }
}