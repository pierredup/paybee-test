<?php

namespace App\Integration\Telegram\Model;

use JMS\Serializer\Annotation as Serializer;

class ChatMessage
{
    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $id;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("first_name")
     *
     * @var string
     */
    private $firstName;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("last_name")
     *
     * @var string
     */
    private $lastName;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param mixed $id
     *
     * @return ChatMessage
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $firstName
     *
     * @return ChatMessage
     */
    public function setFirstName(string $firstName): ChatMessage
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param string $lastName
     *
     * @return ChatMessage
     */
    public function setLastName(string $lastName): ChatMessage
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return ChatMessage
     */
    public function setType(string $type): ChatMessage
    {
        $this->type = $type;

        return $this;
    }
}