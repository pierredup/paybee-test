<?php

namespace App\Integration\Telegram\Model;

use JMS\Serializer\Annotation as Serializer;

class Update
{
    /**
     * @Serializer\SerializedName("update_id")
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $id;

    /**
     * @Serializer\Type("App\Integration\Telegram\Model\Message")
     *
     * @var Message
     */
    private $message;

    public function __construct()
    {
        $this->message = new Message();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @param int $id
     *
     * @return Update
     */
    public function setId(int $id): Update
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param Message $message
     *
     * @return Update
     */
    public function setMessage(Message $message): Update
    {
        $this->message = $message;

        return $this;
    }
}