<?php

namespace App\Integration\Telegram;

use App\Integration\Telegram\Handler\HandlerInterface;
use App\Integration\Telegram\Model\Update;
use Webmozart\Assert\Assert;

class WebHook
{
    /**
     * @var HandlerInterface[]
     */
    private $handlers = [];

    /**
     * @param Update $update
     *
     * @throws \Exception
     */
    public function handle(Update $update)
    {
        Assert::keyExists($this->handlers, $type = (string) $update->getMessage()->getType(), sprintf('There are no handler available to handle "%s" updates', $type));

        $this->handlers[$type]->handle($update);
    }

    /**
     * @param HandlerInterface $handle
     *
     * @throws \Exception
     */
    public function addHandler(HandlerInterface $handle)
    {
        Assert::keyNotExists($this->handlers, $type = $handle->getType(), sprintf('A handle for "%s" is already registered', $type));

        $this->handlers[$type] = $handle;
    }
}