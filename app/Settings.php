<?php

namespace App;

use Illuminate\Database\Query\Builder;

class Settings
{
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public function get(string $key): string
    {
        return $this->builder->where('key', '=', $key)->first()->value;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function set(string $key, string $value): void
    {
        $this->builder->where('key', '=', $key)->update(['value' => $value]);
    }
}