<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class BotConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function __invoke()
    {
        $users = User::where('admin', '=', 0)->paginate(15);

        return view('admin.user-list', ['users' => $users]);
    }
}