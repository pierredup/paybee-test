<?php

namespace App\Http\Controllers\Admin;

use App\Facades\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SaveSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function __invoke(Request $request)
    {
        Settings::set('default_currency', $request->input('currency'));

        $request->session()->flash('status', 'Settings Updated');

        return redirect('/bot-config/settings');
    }
}