<?php

namespace App\Http\Controllers\Admin;

use App\Facades\Settings;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class SettingsController extends Controller
{
    const COINDESK_CACHE_KEY = 'coindesk_currencies';

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function __invoke()
    {
        /** @var \Illuminate\Cache\Repository $store */
        $store = Cache::store('file');

        $currencies = $store->get(self::COINDESK_CACHE_KEY);

        if (!$currencies) {
            $currencies = json_decode(app('coindesk_api')->get('supported-currencies.json')->getBody(), 1);

            $store->add(self::COINDESK_CACHE_KEY, $currencies, Carbon::now()->addDay());
        }

        $defaultCurrency = Settings::get('default_currency');

        return view('admin.settings', ['currencies' => $currencies, 'defaultCurrency' => $defaultCurrency]);
    }
}