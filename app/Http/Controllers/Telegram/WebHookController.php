<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Integration\Telegram\Model\Update;
use App\Integration\Telegram\WebHook;
use Illuminate\Http\Request;
use JMS\Serializer\Serializer;

class WebHookController extends Controller
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var WebHook
     */
    private $webHook;

    public function __construct(Serializer $serializer, WebHook $webHook)
    {
        $this->serializer = $serializer;
        $this->webHook = $webHook;
    }

    public function __invoke(Request $request)
    {
        /** @var Update $update */
        $update = $this->serializer->deserialize($request->getContent(), Update::class, 'json');

        $this->webHook->handle($update);
    }
}