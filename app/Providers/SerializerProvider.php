<?php

namespace App\Providers;

use App\Integration\Telegram\Model\Type;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Illuminate\Support\ServiceProvider;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;

class SerializerProvider extends ServiceProvider
{
    public function register()
    {
        AnnotationRegistry::registerAutoloadNamespace(
            'JMS\Serializer\Annotation', realpath(__DIR__.'/../../vendor/jms/serializer/src')
        );

        $this->app->bind(Serializer::class, function () {
            return SerializerBuilder::create()
                ->setCacheDir(storage_path('framework/cache/serializer'))
                ->setDebug($this->app['config']['app.debug'])
                ->configureHandlers(function (HandlerRegistry $registry) {
                    $registry->registerSubscribingHandler(new class implements SubscribingHandlerInterface
                    {
                        public static function getSubscribingMethods(): array
                        {
                            return [
                                [
                                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                                    'format' => 'json',
                                    'type' => Type::class,
                                    'method' => 'deserialize',
                                ],
                            ];
                        }

                        public function deserialize(JsonDeserializationVisitor $visitor, array $data): Type
                        {
                            return new Type($data[0]);
                        }
                    });
                })
                ->build();
        });
    }

    public function provides()
    {
        return [
            'serializer',
        ];
    }
}