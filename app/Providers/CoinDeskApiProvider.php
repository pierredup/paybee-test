<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class CoinDeskApiProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('coindesk_api', function () {
            return new Client(['base_uri' => $this->app['config']['coindesk']['url']]);
        });
    }
}