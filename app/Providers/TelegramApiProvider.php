<?php

namespace App\Providers;

use App\Api\MiddleWare\TelegramHttpError;
use App\Api\Telegram;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\ServiceProvider;
use Webmozart\Assert\Assert;

class TelegramApiProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('telegram_api', function () {
            $config = $this->app['config']['telegram'];

            $api = $config['api'] ?? [];

            Assert::keyExists($config, 'auth_token');
            Assert::keyExists($api, 'base_uri');

            $api['base_uri'] .= $config['auth_token'].'/';

            $api['http_errors'] = false;
            $api['handler'] = $this->buildHttpHandler();

            return new Client($api);
        });

        $this->app->singleton(Telegram::class, function () {
            return new Telegram($this->app->make('telegram_api'));
        });
    }

    private function buildHttpHandler()
    {
        $handler = HandlerStack::create();
        $handler->push(new TelegramHttpError);

        return $handler;
    }
}