<?php

namespace App\Providers;

use App\Facades\Settings;
use App\Integration\Telegram\Handler\BotCommandHandler;
use App\Integration\Telegram\Handler\BotCommands\ConnectCommand;
use App\Integration\Telegram\Handler\BotCommands\GetBTCEquivalent;
use App\Integration\Telegram\Handler\BotCommands\GetUserId;
use App\Integration\Telegram\Handler\BotCommands\StartCommand;
use App\Integration\Telegram\WebHook;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class WebHookProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('bot_command.handler', function () {
            $commands = [
                'getbtcequivalent' => new GetBTCEquivalent($this->app->make('coindesk_api'), Settings::get('default_currency')),
                'start' => new StartCommand(DB::table('users'), $this->app['config']['app']['url']),
                'connect' => new ConnectCommand(DB::table('users')),
                'getuserid' => new GetUserId(DB::table('users')),
            ];

            return new BotCommandHandler($commands);
        });

        $this->app->bind(WebHook::class, function () {
            $webHook = new WebHook();
            $webHook->addHandler($this->app->make('bot_command.handler'));

            return $webHook;
        });
    }
}