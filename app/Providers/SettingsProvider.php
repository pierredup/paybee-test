<?php

namespace App\Providers;

use App\Settings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class SettingsProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(Settings::class, function () {
            return new Settings(DB::table('settings'));
        });
    }
}