<?php

namespace App\Exception;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class TelegramRequestException extends RequestException
{
    public function __construct(RequestInterface $request, ResponseInterface $response)
    {
        $responseContent = json_decode($response->getBody(), true);

        parent::__construct('Telegram API Exception: ' . $responseContent['description'], $request, $response);
    }
}