<?php

namespace App\Api;

use App\Exception\TelegramRequestException;
use GuzzleHttp\Client;
use Webmozart\Assert\Assert;

class Telegram
{
    private $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param int    $chatId
     * @param string $message
     *
     * @return array
     * @throws TelegramRequestException|\InvalidArgumentException
     */
    public function sendMessage(int $chatId, string $message): ?array
    {
        Assert::greaterThan($chatId, 0);
        Assert::notEmpty($message);

        $response = $this->httpClient->get('sendMessage', ['query' => ['chat_id' => $chatId, 'text' => $message]]);

        return json_decode($response->getBody(), true);
    }
}