<?php

namespace App\Api\MiddleWare;

use App\Exception\TelegramRequestException;
use Psr\Http\Message\ResponseInterface;

class TelegramHttpError
{
    public function __invoke(callable $handler)
    {
        return function ($request, array $options) use ($handler) {
            return $handler($request, $options)->then(
                function (ResponseInterface $response) use ($request, $handler) {
                    $code = $response->getStatusCode();
                    if ($code < 400) {
                        return $response;
                    }

                    throw new TelegramRequestException($request, $response);
                }
            );
        };
    }
}