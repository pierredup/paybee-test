@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Settings</div>

        <div class="panel-body">
            <form method="post" action="{{ route('admin.settings.save') }}">
                <div class="form-group">
                    <label class="control-label">Default Currency</label>
                    <select name="currency" class="form-control">
                        <option>Please select the default currency</option>
                        @foreach($currencies as $currency)
                            <option value="{{ $currency['currency'] }}" @if($currency['currency'] === $defaultCurrency) selected @endif>{{ $currency['currency'] }}</option>
                        @endforeach
                    </select>
                </div>

                {{ csrf_field() }}

                <input type="submit" class="btn btn-primary">
            </form>
        </div>
    </div>
@endsection
