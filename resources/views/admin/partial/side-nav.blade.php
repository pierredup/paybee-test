<div class="list-group">
    <a href="{{ route('admin.bot_config') }}" class="list-group-item{{ Request::is('bot-config') ? ' active' : ''}}">
        <i class="fa fa-comment-o"></i> Users
    </a>
    <a href="{{ route('admin.settings') }}" class="list-group-item{{ Request::is('bot-config/settings') ? ' active' : ''}}">
        <i class="fa fa-search"></i> Settings
    </a>
</div>