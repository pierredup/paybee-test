@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (empty($user->telegram_id))
                            <div class="alert alert-danger">
                                You are not connected to Telegram.
                            </div>

                            <h3>How to connect to Telegram</h3>

                            <p>In order to connect to Telegram, you need to add the <a href="https://telegram.me/PayBeeBot">@PayBeeBot</a>.</p>
                            <p>Once you have added the <a href="https://telegram.me/PayBeeBot">@PayBeeBot</a>, send the following command to authorize your account:</p>
                            <p>
                                <code>/connect {{ $user->token }}</code>
                            </p>

                            <p>
                                Once your PayBee account is connected to your Telegram account, you can start sending sending commands.
                            </p>

                            <h4>Telegram Authentication Code</h4>

                            <pre>

                                {{ $user->token }}
                            </pre>

                            <p>
                                <a href="https://telegram.me/PayBeeBot" class="btn btn-success btn-lg" rel="external">
                                    Add Telegram Bot
                                </a>
                            </p>
                        @else
                            <div class="alert alert-success">
                                Your account is connected to Telegram
                            </div>

                            <dl class="dl-horizontal">
                                <dt>Telegram Id</dt>
                                <dd>{{ $user->telegram_id }}</dd>
                            </dl>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
